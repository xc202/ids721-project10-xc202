use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use rust_bert::pipelines::conversation::{ConversationModel, ConversationManager};
use tracing_subscriber::filter::{EnvFilter, LevelFilter};

#[derive(Deserialize)]
pub struct Request {
    pub text: String,
}

#[derive(Serialize)]
pub struct Response {
    pub answer: String,
}


async fn request_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let text = event.payload.text.clone();

    let conversation_model = ConversationModel::new(Default::default());
    let mut conversation_manager = ConversationManager::new();

    let _conversation_id = conversation_manager.create(&*text);
    let output = conversation_model.expect("Model collapse").generate_responses(&mut conversation_manager);
    let map = output.unwrap();
    let mut response = None;
    for (_key, value) in map {
        response = Some(Response {
            answer: value.parse().unwrap(),
        });
    }
    if let Some(response) = response {
        Ok(response)
    } else {
        Err(Error::from("Failed to generate response"))
    }
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(request_handler)).await
}


