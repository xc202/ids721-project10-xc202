## Rust Serverless Transformer Endpoint

The purpose of this project is to Dockerize a Hugging Face Rust transformer and deploy it as a container to AWS Lambda. Subsequently, a query endpoint will be implemented to allow users to query the transformer by sending requests. The project will package the transformer into a Docker container using a Dockerfile and Rust code, deploy the container to AWS Lambda, and implement an endpoint functionality allowing users to query the transformer via cURL requests, accompanied by appropriate documentation.

### Code Overview

The Rust code snippet demonstrates a Lambda function implemented with the `lambda_runtime`, `serde`, and `rust_bert` crates. Download Libtorch [here](https://pytorch.org/) to run.

#### Source:
The `rust_bert` crate is a Rust implementation of the Hugging Face Transformers library, providing access to pre-trained natural language processing models. You can find more information about `rust_bert` [here](https://github.com/guillaume-be/rust-bert?tab=readme-ov-file).

#### Functionality:
- The Lambda function receives an event containing a text string.
- It initializes a conversation model and manager from the `rust_bert::pipelines::conversation` module.
- The function creates a conversation with the received text, generates responses using the conversation model, and returns the generated response as a string in the response object.

#### Important Notice
In the `Cargo.toml` file, the version of `rust-bert` is the GitHub version, not the v0.22.0 release version. The GitHub version allows the use of the latest `libtorch 2.2.0+` version, while the release version is only compatible with version `2.1.0`. If you want to bypass the version check, set `LIBTORCH_BYPASS_VERSION_CHECK=true`.

Additionally, for `arm64` and `x86-64` architectures, it is necessary to differentiate the downloaded `libtorch` versions. For systems supporting the `arm64` architecture (including the Apple M1 chip), it is preferable to use `libtorch 2.2.0+` version, and set `rust-bert` to the GitHub version.

```shell
rust-bert = { git = "https://github.com/guillaume-be/rust-bert.git", branch = "main" }
torch-sys = { version = "0.15.0", features = ["download-libtorch"] }
```

### Test Locally

1. Install Cargo Lambda

Ensure you have Cargo Lambda installed. You can install it using Cargo:
```shell
cargo install cargo-lambda
```

2. Compile Lambda Function

Compile your Lambda function using Cargo Lambda:
```shell
cargo lambda build
```

3. Invoke Lambda Locally

You can test your Lambda function locally by invoking it with sample events. For example:
```shell
cargo lambda invoke --event '{"text": "Hello, world!"}'
```

4. Alternatively, use the following code and `curl` to test locally:

```shell
cargo lambda watch
```

### Docker Deployment

1. Navigate to `Amazon ECR` under the Amazon console and create a new repository.
2. Retrieve the login password for AWS ECR in the us-east-1 region and uses it to authenticate Docker login to the specified ECR repository.
```shell
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <account-number>.dkr.ecr.us-east-1.amazonaws.com
```
3. Build a Docker image named "tf" using Docker Buildx. Buildx is a Docker CLI plugin that extends the Docker build command to support multiple platforms, making it easier to build images for different architectures.
```shell
docker buildx build -t tf .
```
4. Tag a Docker image with the "latest" tag for pushing to an ECR repository in the us-east-1 region.
```shell
docker tag tf:latest <account-number>.dkr.ecr.us-east-1.amazonaws.com/<respository-name>
```
5. Push a Docker image to an Amazon ECR repository located in the us-east-1 region. <account-number> represents your AWS account number, and <repository-name> is the name of the repository where you want to push the image.
```shell
docker push <account-number>.dkr.ecr.us-east-1.amazonaws.com/<respository-name>
```

![Screenshot 2024-04-13 at 8.41.51 PM.png](screenshots%2FScreenshot%202024-04-13%20at%208.41.51%20PM.png)

### AWS Lambda

Create a new AWS Lambda function based on the pre-uploaded container image. Be sure to choose the right architecture based on your running environment. Use `uname -a` to check it.

![Screenshot 2024-04-13 at 8.42.25 PM.png](screenshots%2FScreenshot%202024-04-13%20at%208.42.25%20PM.png)

Due to the relatively long execution time of large models, remember to increase the function's usage capacity and timeout.

![Screenshot 2024-04-14 at 8.48.34 AM.png](screenshots%2FScreenshot%202024-04-14%20at%208.48.34%20AM.png)

### Results

![Screenshot 2024-04-14 at 8.48.59 AM.png](screenshots%2FScreenshot%202024-04-14%20at%208.48.59%20AM.png)
