FROM rust:latest as builder

WORKDIR /app

ADD . ./

RUN wget https://download.pytorch.org/libtorch/cpu/libtorch-cxx11-abi-shared-with-deps-2.2.2%2Bcpu.zip
RUN unzip libtorch-cxx11-abi-shared-with-deps-2.2.2+cpu.zip -d ./torch_lib/

ENV LIBTORCH=/app/torch_lib/libtorch
ENV LD_LIBRARY_PATH=${LIBTORCH}/lib
ENV LIBTORCH_BYPASS_VERSION_CHECK=true
ENV DYLD_LIBRARY_PATH=${LIBTORCH}/lib

RUN cargo clean && cargo build --release

CMD ["/app/target/release/rust_serverless_transformer_endpoint"]
